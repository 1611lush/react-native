/**
 * @template T
 * @param {(element: T) => boolean} predicate
 * @param {T[]} collection
 * @returns {T[]}
 */

const realiseFilter = function(predicate, collection) {
	let mappedArray = collection.reduce(function(accumulator, currentValue) {
		if(predicate(currentValue)){
			return [...accumulator, currentValue]
		}
		return accumulator		
	}, [])	
	return mappedArray
}