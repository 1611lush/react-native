const Variables = {
  accentColor: '#373AF5',
  secondaryColor: '#8083A4',
  basestrongColor: '#2C2D76',
  criticColor: '#E5596D',
  baseweakColor: '#FFFFFF',
};

export default Variables;
