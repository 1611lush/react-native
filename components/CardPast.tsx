import * as React from 'react';
import {API_KEY, BASE_URL} from '../data/config';
import {City} from '../data/helpers';
import { Text, View, StyleSheet, TextInput, ScrollView, Modal } from 'react-native';
import Variables from "../Variables";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import { DatePicker } from 'react-native-woodpicker'
import '@expo/match-media';
import MediaQuery from 'react-responsive'
import { useMediaQuery } from 'react-responsive'


interface CardProps {
	title: string,	
	cityArray: City[],
	apiRequest: (url:string) => void
}


const CardPast: React.FC<CardProps> = (props: CardProps) => {

	const urlPast = 'data/2.5/onecall/timemachine?'
	const [cityText, setCityText] =  React.useState('Select city')

	const getCityElements:City[]  = [...props.cityArray]

	const [visibleCityList, setVisibleCityList] = React.useState(false);
	const toggleCities = () => setVisibleCityList(!visibleCityList);


	const [openDropdown, setOpenDropdown] =  React.useState(false);
	const toggleOpenDropdownClass = () => {
		setOpenDropdown(!openDropdown);
	};	

	const [choosenDay, setChoosenDay] = React.useState<number>()

	const [latInput, setLatInput] = React.useState<number>()
	const [lonInput, setLonInput] = React.useState<number>()

	
	const makeRequestWithDate = (target: any) => {
		const formattedDate = parseInt((new Date(target.toString()).getTime() / 1000).toFixed(0))
		setChoosenDay(formattedDate)
		props.apiRequest(`${BASE_URL}${urlPast}lat=${latInput}&lon=${lonInput}&dt=${formattedDate}&appid=${API_KEY}&units=metric`)	
	}
	const makeRequest = (lat: number, lon: number) => {		
		choosenDay && props.apiRequest(`${BASE_URL}${urlPast}lat=${lat}&lon=${lon}&dt=${choosenDay}&appid=${API_KEY}&units=metric`)	
	}

	const maxDate = new Date()
	const fivePastDays = Date.now() - 432000000
	const minDate = new Date(fivePastDays)
	

	const onPressTitle = () => {
		setModalVisible(true)
	  	toggleCities()
		toggleOpenDropdownClass() 	
	}

    const [fontsLoaded] = useFonts({
     	'Ubuntu_400Regular': require('../assets/fonts/Ubuntu-Regular.ttf'),
    	'Ubuntu_700Bold': require('../assets/fonts/Ubuntu-Bold.ttf'),
	});
   
    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 992px)' })
 	const [modalVisible, setModalVisible] = React.useState(false);

    if (!fontsLoaded) {
	   return <AppLoading />;
	} else {


		return(

			<React.Fragment>
	
				<Text style={styles.forecast__title}>
					{props.title}
				</Text>
				
				<View style={!isTabletOrMobile ? styles.forecast__inputs : styles.forecast__inputs_mobile}>
					
					<View style={openDropdown ? styles.forecast__select_up : styles.forecast__select} >
				
						<Text  style={styles.forecast__select_text} onPress={()=>{onPressTitle()}}>
							{cityText}
						</Text>

						{visibleCityList &&
							<Modal  visible={modalVisible} style={styles.forecast__cities} >						
								
								{getCityElements.map((cityElement: {name: string, lat: number, lon: number}) => {

									return (
										<View key={cityElement.name} style={styles.forecast__cities_inner}>
											<Text 
 												onPress={								
													() => {
														setCityText(cityElement.name)	
														setVisibleCityList(!visibleCityList)
														toggleOpenDropdownClass() 
														setLatInput(cityElement.lat)
														setLonInput(cityElement.lon)
														makeRequest(cityElement.lat, cityElement.lon)
														setModalVisible(!modalVisible)
													}				
												}>{cityElement.name} </Text>
											</View>
										)						

									})			
								}

							</Modal>
						}

					</View>		
					
			
					
						
						
						<DatePicker  
					        onDateChange={target =>makeRequestWithDate(target)}
					        title="Date Picker"
					        text='Select date'
					        isNullable
					        style={styles.forecast__select}
						    maximumDate={maxDate} 
							minimumDate={minDate}
					        iosDisplay="inline"/>
							

				</View>	

			</ React.Fragment>
		)
	}

}

const styles = StyleSheet.create({
    forecast__title: {
		fontFamily: 'Ubuntu_700Bold',
		fontSize: 32,
		lineHeight: 32,
		color: Variables.basestrongColor,
    },
     forecast__select_text: {
		color: Variables.secondaryColor,
		fontFamily: 'Ubuntu_400Regular',
		fontSize: 16,
		lineHeight: 20,
    }, 
    forecast__inputs: {
    	display: 'flex',
    	flexDirection: 'row',
    	justifyContent: 'space-between', 
    },
    forecast__inputs_mobile: {
    	display: 'flex',
    	flexDirection: 'column',    	
    	zIndex: 3,
    },
    forecast__select: {
		marginTop: 32,
		lineHeight: 20,
		backgroundColor: 'rgba(128, 131, 164, 0.06)',
		borderStyle: 'solid',
		borderWidth: 2,
		borderColor: 'rgba(128, 131, 164, 0.2)',
		borderRadius: 8,
		paddingVertical: 12,
		paddingHorizontal: 16,
		maxWidth: 252,
		maxHeight: 48,
    	zIndex: 4,
    	position: 'relative'
    },  
    forecast__select_up: {
		marginTop: 32,
		color: Variables.secondaryColor,
		backgroundColor: 'rgba(128, 131, 164, 0.06)',
		borderStyle: 'solid',
		borderWidth: 2,
		borderColor: 'rgba(128, 131, 164, 0.2)',
		borderRadius: 8,
		paddingVertical: 12,
		paddingHorizontal: 16,
		maxWidth: 252,
		maxHeight: 48,
    	zIndex: 4,
    },
    forecast__cities: {
		margin: 0,
		position: 'absolute',
		backgroundColor: '#FFFFFF',
		borderStyle: 'solid',
		borderWidth: 2,
		borderColor: 'rgba(128, 131, 164, 0.06)',
		shadowColor: "#040549",
		shadowOffset: {
			width: 2,
			height: 4,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		borderRadius: 8,
		padding: 6,
		width: '100%',
		maxWidth: 233,
		fontFamily: 'Ubuntu_400Regular',
		fontStyle: 'normal',
		fontSize: 16,
		lineHeight: 24,
		color: Variables.basestrongColor,
		height: 'auto',
		top: 60,
		left: 0,
    	zIndex: 15,
    },
      forecast__cities_inner: {    	
		paddingVertical: 6,
		paddingHorizontal: 10,	
		zIndex: 6
    },
    forecast__cities_text: {
		fontFamily: 'Ubuntu_400Regular',
		fontStyle: 'normal',
		fontSize: 16,
		lineHeight: 20,

    }, 


})
export default CardPast