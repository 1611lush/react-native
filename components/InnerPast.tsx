import * as React from 'react';
import {weatherInterface} from '../data/helpers';
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Variables from "../Variables";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';

interface InnerPastProps {
	weatherDataProps?: weatherInterface
}



const InnerPast: React.FC<InnerPastProps> = (props: InnerPastProps) => {
	const getValueFromProps = Object.values(props.weatherDataProps!)
	const monthNames: string[] = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
	const getDateFromProps = new Date(getValueFromProps[0]*1000).toLocaleString("en-US").split(",")[0]	
	const getMonthName: string = getDateFromProps.split("/")[0]
	const getFinalDate: string = (getDateFromProps.split("/")[1] + ' ' + monthNames[+getMonthName - 1] + ' ' + getDateFromProps.split("/")[2])
	
	const fixedDate = new Date(getValueFromProps[0]*1000)
	const nativeDate = fixedDate.getDate() + ' ' + monthNames[(fixedDate.getMonth())] + ' ' + fixedDate.getFullYear()

    let [fontsLoaded] = useFonts({
     	'Ubuntu_400Regular': require('../assets/fonts/Ubuntu-Regular.ttf'),
    	'Ubuntu_700Bold': require('../assets/fonts/Ubuntu-Bold.ttf'),
	});

    if (!fontsLoaded) {
	   return <AppLoading />;
	} else {

		return(	

				<View style={styles.forecast__result_full}>						
					<Text style={styles.forecast__date}>{nativeDate}</Text>	
					<Image source={{uri:`http://openweathermap.org/img/wn/${getValueFromProps[2]}@2x.png`}}  style={styles.forecast__img} />
					<Text style={styles.forecast__temp}>+{getValueFromProps[1]}°</Text>
				</View>	

		)
	}
}
		
const styles = StyleSheet.create({
    forecast__result_full: {
		textAlign: 'center',
		marginTop: 54,
		marginRight: 10,
		backgroundColor: Variables.accentColor,
		borderStyle: 'solid',
		borderWidth: 2,
		borderColor: '#373AF5',
		shadowColor: "#040549",
		shadowOffset: {
			width: 0,
			height: 4,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		borderRadius: 8,
		padding: 20,
		minHeight: 240,
    	zIndex: 0,
    	elevation: 0
    },
    forecast__date: {
	 	fontFamily: 'Ubuntu_700Bold',
		color: Variables.baseweakColor,
		fontSize: 16,
		lineHeight: 21,
		textAlign: 'left'
    },
    forecast__temp: {
		fontFamily: 'Ubuntu_700Bold',
		color: Variables.baseweakColor,
		fontSize: 32,
		lineHeight: 34,
		textAlign: 'right',
    },
    forecast__img: {
    	width: 120,
	  	height: 120,
	    marginTop: 15,
	    alignSelf:'center',
	    marginBottom: 15,
    }
})

export default InnerPast