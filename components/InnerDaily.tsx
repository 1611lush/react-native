import * as React from 'react';
import {weatherInterface} from '../data/helpers';
import { Text, View, StyleSheet, TouchableOpacity, Image, ScrollView } from 'react-native';
import Variables from "../Variables";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';

interface InnerDailyProps {
	weatherDataProps?: weatherInterface	
}



const InnerDaily: React.FC<InnerDailyProps> = (props: InnerDailyProps) => {	
	const monthNames: string[] = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
	const getValueFromProps = Object.values(props.weatherDataProps!)

	const showWeatherOnWeekday = getValueFromProps[0].map((weatherOnWeekday: number, index: number) => {
		const getDateFromProps = new Date(weatherOnWeekday*1000).toLocaleString("en-US").split(",")[0]	
		const getMonthName: string = getDateFromProps.split("/")[0] 		
		const getFinalDate: string = (getDateFromProps.split("/")[1] + ' ' + monthNames[+getMonthName - 1] + ' ' + getDateFromProps.split("/")[2])

		const fixedDate = new Date(weatherOnWeekday*1000)
		const nativeDate = fixedDate.getDate() + ' ' + monthNames[(fixedDate.getMonth())] + ' ' + fixedDate.getFullYear()

		return (
			<View style={styles.forecast__result_full} key={index}>
				<Text style={styles.forecast__date}>{nativeDate}</Text>
				<Image source={{uri:`http://openweathermap.org/img/wn/${getValueFromProps[2][index]}@2x.png`}}  style={styles.forecast__img} />
				<Text style={styles.forecast__temp}>+{getValueFromProps[1][index]}°</Text>
			</View>
		)
	})
		

		
	const [margin, setMargin] = React.useState(0);

    let [fontsLoaded] = useFonts({
     	'Ubuntu_400Regular': require('../assets/fonts/Ubuntu-Regular.ttf'),
    	'Ubuntu_700Bold': require('../assets/fonts/Ubuntu-Bold.ttf'),
	});

    if (!fontsLoaded) {
	   return <AppLoading />;
	} else {

		return(	
		
				<View style={styles.slider}>
							
					<ScrollView  horizontal={true} style={{
						marginLeft : margin,
						position: 'relative',
						paddingBottom: 20,
						}}
					>

						{
							showWeatherOnWeekday
						}	

					</ScrollView>
	
				</View>

		)
	}

}
	
const styles = StyleSheet.create({
    forecast__result_full: {
		textAlign: 'center',
		marginTop: 54,
		marginRight: 10,
		backgroundColor: Variables.accentColor,
		borderStyle: 'solid',
		borderWidth: 2,
		borderColor: '#373AF5',
		shadowColor: "#040549",
		shadowOffset: {
			width: 0,
			height: 4,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		borderRadius: 8,
		padding: 20,
		minHeight: 240,
    },
    forecast__date: {
		fontStyle: 'normal',
		fontFamily: 'Ubuntu_700Bold',
		color: Variables.baseweakColor,
		fontSize: 16,
		lineHeight: 21,
		textAlign: 'left'
    },
    forecast__temp: {
		fontStyle: 'normal',
		fontFamily: 'Ubuntu_700Bold',
		color: Variables.baseweakColor,
		fontSize: 32,
		lineHeight: 34,
		textAlign: 'right',
    },
    forecast__img: {
		width: 120,
		height: 120,
		marginTop: 15,
	    alignSelf:'center',
		marginBottom: 15,
    },
    slider: {
		zIndex: 0,
		overflow: 'hidden',
		position: 'relative',
		paddingLeft: 26,
		marginLeft: -28,
		paddingRight: 23,
		marginRight: -30,
		marginBottom: 0,
    },
})


export default InnerDaily