import * as React from 'react';
import {API_KEY, BASE_URL} from '../data/config';
import {City} from '../data/helpers';
import { Text, View, StyleSheet, ScrollView, Modal } from 'react-native';
import Variables from "../Variables";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';


interface CardProps {
	title: string,
	cityArray: City[],
	apiRequest: (url: string) => void
}

const CardDaily: React.FC<CardProps> = (props: CardProps) => {

	const urlDaily = 'data/2.5/onecall?'
	const [cityText, setCityText] =  React.useState('Select city')

	const getCityElements:City[]  = [...props.cityArray]

	const [visibleCityList, setVisibleCityList] = React.useState(false);
	const toggleCities = () => setVisibleCityList(!visibleCityList);

	const [openDropdown, setOpenDropdown] =  React.useState(false);
	const toggleClass = () => {
		setOpenDropdown(!openDropdown);
	};

	const makeRequest = (lat: number, lon: number) => {		
		props.apiRequest(`${BASE_URL}${urlDaily}lat=${lat}&lon=${lon}&exclude=current,minutely,hourly,alerts&appid=${API_KEY}&units=metric`)	
	}

	const onPressTitle = () => {
		setModalVisible(true)
	    toggleCities()
		toggleClass() 	
	}

    let [fontsLoaded] = useFonts({
     	'Ubuntu_400Regular': require('../assets/fonts/Ubuntu-Regular.ttf'),
    	'Ubuntu_700Bold': require('../assets/fonts/Ubuntu-Bold.ttf'),
	});

 	const [modalVisible, setModalVisible] = React.useState(false);

    if (!fontsLoaded) {
	   return <AppLoading />;
	} else {



		return(

			<React.Fragment>
				
				<Text style={styles.forecast__title}>
					{props.title}
				</Text>
				
				<View style={openDropdown ? styles.forecast__select_up : styles.forecast__select} >
				
					<Text  style={styles.forecast__select_text} onPress={()=>{onPressTitle()}}>
						{cityText}
					</Text>

					{visibleCityList &&
						<Modal visible={modalVisible}  style={styles.forecast__cities}  >
				      
				

							{
								getCityElements.map((cityElement: {name: string, lat: number, lon: number}) => {
									return (
										<View key={cityElement.name} style={styles.forecast__cities_inner}>
											<Text 
 												style={styles.forecast__cities_text}
 												key={cityElement.name} 
												onPress={				
													() => {
														setCityText(cityElement.name)	
														setVisibleCityList(!visibleCityList)
														toggleClass() 
														makeRequest(cityElement.lat, cityElement.lon)
														setModalVisible(!modalVisible)
													}								
												}>{cityElement.name}
											</Text>
										</View>
									)						

								})			
							}
						</Modal>
					}

				</View>			

			</ React.Fragment>

		)
	}
}


const styles = StyleSheet.create({
    forecast__title: {
		fontFamily: 'Ubuntu_700Bold',
		fontStyle: 'normal',
		fontWeight: 'bold',
		fontSize: 32,
		lineHeight: 32,
		color: Variables.basestrongColor,
    },
    forecast__select_text: {
		color: Variables.secondaryColor,
		fontFamily: 'Ubuntu_400Regular',
		fontStyle: 'normal',
		fontWeight: 'normal',
		fontSize: 16,
		lineHeight: 20,
    },
    forecast__select: {
		marginTop: 32,
		lineHeight: 20,
		backgroundColor: 'rgba(128, 131, 164, 0.06)',
		borderStyle: 'solid',
		borderWidth: 2,
		borderColor: 'rgba(128, 131, 164, 0.2)',
		borderRadius: 8,
		paddingVertical: 12,
		paddingHorizontal: 16,
		maxWidth: 252,
		maxHeight: 48,
		zIndex: 3,
    },  
    forecast__select_up: {
		marginTop: 32,
		color: Variables.secondaryColor,
		backgroundColor: 'rgba(128, 131, 164, 0.06)',
		borderStyle: 'solid',
		borderWidth: 2,
		borderColor: 'rgba(128, 131, 164, 0.2)',
		borderRadius: 8,
		paddingVertical: 12,
		paddingHorizontal: 16,
		maxWidth: 252,
		maxHeight: 48,
		zIndex: 3,
    },
    forecast__cities: {
		margin: 0,
		position: 'absolute',
		backgroundColor: '#FFFFFF',
		borderStyle: 'solid',
		borderWidth: 2,
		borderColor: 'rgba(128, 131, 164, 0.06)',
		shadowColor: "#040549",
		shadowOffset: {
			width: 2,
			height: 4,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		borderRadius: 8,
		padding: 6,
		width: '100%',
		maxWidth: 233,
		fontFamily: 'Ubuntu_400Regular',
		fontStyle: 'normal',
		fontSize: 16,
		lineHeight: 24,
		color: Variables.basestrongColor,
		top: 60,
		left: 0,		
    	zIndex: 100,
    	height: 142
    },
      forecast__cities_inner: {    	
		paddingVertical: 6,
		paddingHorizontal: 10,	
    },
    forecast__cities_text: {
		fontFamily: 'Ubuntu_400Regular',
		fontSize: 16,
		lineHeight: 20,
    }
})



export default CardDaily