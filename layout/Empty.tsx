import * as React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import Variables from "../Variables";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import '@expo/match-media';
import MediaQuery from 'react-responsive'
import { useMediaQuery } from 'react-responsive'



const styles = StyleSheet.create({
  forecast__result_empty: {
    marginTop: 63,
 	marginBottom: 42,
 	textAlign: 'center',
 	justifyContent: 'center',
 	zIndex: -1 
  },
  forecast__text: {
  	fontFamily: 'Ubuntu_700Bold',
	fontSize: 16,
	lineHeight: 24,
	color: Variables.secondaryColor,
	textAlign: 'center'
  },
  forecast__result_empty_mobile: {
	marginTop: 0,
 	marginBottom: 17,
  }
})


const Empty: React.FC = () => {
    let [fontsLoaded] = useFonts({
     	'Ubuntu_400Regular': require('../assets/fonts/Ubuntu-Regular.ttf'),
    	'Ubuntu_700Bold': require('../assets/fonts/Ubuntu-Bold.ttf'),
	});
	
	const isTabletOrMobile = useMediaQuery({ query: '(max-width: 992px)' })


    if (!fontsLoaded) {
	   return <AppLoading />;
	} else {

		return (

		    <View style={!isTabletOrMobile ? styles.forecast__result_empty : styles.forecast__result_empty_mobile} >	
				<Image
		          style={{
		            width: 160,
					height: 160,				
					marginTop: 10,
					marginBottom: 25,
					alignSelf: 'center'
		          }}
		            source={{
			          uri: 'https://1611lush.github.io/testRepo/images/Academy-Weather-bg160.png',
			        }}
		        />
				<Text style={styles.forecast__text}>Fill in all the fields and the weather will be displayed</Text>
			</View>  
		)
	}
}


export default Empty