import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Variables from "../Variables";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';

const styles = StyleSheet.create({
  footer: {
    opacity: 0.6,
    marginTop: 199,
    paddingBottom: 16,
    textAlign: 'center',
    position: 'relative'
  },
  footer_text: {
    textAlign: 'center',
    fontFamily: 'Ubuntu_400Regular',
    fontSize: 14,
    lineHeight: 18,
    color: Variables.baseweakColor,
  }
})


const Footer: React.FC = () => {
    let [fontsLoaded] = useFonts({
        'Ubuntu_400Regular': require('../assets/fonts/Ubuntu-Regular.ttf'),
        'Ubuntu_700Bold': require('../assets/fonts/Ubuntu-Bold.ttf'),
    });

    if (!fontsLoaded) {
       return <AppLoading />;
    } else {


      return (
        <View style={styles.footer}>	
    		<Text style={styles.footer_text}>C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT</Text>
    	</View>
    )}
}

export default Footer;


