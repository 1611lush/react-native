import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import {useWindowDimensions} from 'react-native';
import Variables from "../Variables";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import '@expo/match-media';
import MediaQuery from 'react-responsive'
import { useMediaQuery } from 'react-responsive'


const Header: React.FC = () => {
    let [fontsLoaded] = useFonts({
     	'Ubuntu_400Regular': require('../assets/fonts/Ubuntu-Regular.ttf'),
    	'Ubuntu_700Bold': require('../assets/fonts/Ubuntu-Bold.ttf'),
	});

    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 992px)' })

    if (!fontsLoaded) {
	   return <AppLoading />;
	} else {

	return (

		<View style={!isTabletOrMobile ? styles.header : styles.header_mobile}>
			<Text style={!isTabletOrMobile ? styles.header__text : styles.header__text_mobile}>
				<Text style={!isTabletOrMobile ? styles.header__title_left : styles.header__title_left_mobile}>Weather</Text>
				<Text style={!isTabletOrMobile ? styles.header__title_right : styles.header__title_right_mobile}>forecast</Text>
			</Text>
		</View>

	)}
}


const styles = StyleSheet.create({
    header: {
		paddingTop: 65,
		paddingBottom: 75,
		maxWidth: 825,
		margin: 'auto',	
		width: 825,
		justifyContent: 'center',
		position: 'relative',		
    },
    header_mobile: {
     	paddingTop: 33,
		paddingBottom: 24,
		width: 260,		
		maxWidth: 260,
		alignSelf: 'center',
    },
    header__text: {
		fontFamily: 'Ubuntu_700Bold',
		fontStyle: 'normal',
		textAlign: 'center',
		color: Variables.baseweakColor,
		fontSize: 102,
		lineHeight: 91,
		flexDirection: 'column',
		justifyContent: 'center',	
		display: 'flex'
    },
    header__text_mobile: {
    	fontFamily: 'Ubuntu_700Bold',
		fontStyle: 'normal',
		textAlign: 'center',
		color: Variables.baseweakColor,
		fontSize: 32,
		lineHeight: 32,
		display: 'flex',
		flexDirection: 'column',
    },
    header__title_left: {
		marginLeft: 'calc(-50% + 55px)'
    },
    header__title_right: {
		marginLeft: 'calc(50% - 45px)'
    },
    header__title_left_mobile: {
		marginLeft: 'calc(-50%  + 17px)'
    },
    header__title_right_mobile: {
		marginLeft: 'calc(-50% - 17px)'
    }
})


export default Header
