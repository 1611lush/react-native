import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground, ScrollView, useWindowDimensions } from 'react-native';
import Header from './layout/Header';
import Footer from './layout/Footer';
import Empty from './layout/Empty';
import CardDaily from './components/CardDaily';
import CardPast from './components/CardPast';
import InnerPast from './components/InnerPast';
import InnerDaily from './components/InnerDaily';
import {cities, weatherInterface} from './data/helpers';
import Variables from "./Variables";
import '@expo/match-media';
import MediaQuery from 'react-responsive'
import { useMediaQuery } from 'react-responsive'


export default function App() {

	const [errorDailyWeather, setErrorDailyWeather] = React.useState('')  
	const [errorPastWeather, setErrorPastWeather] = React.useState('')
	const [dailyWeatherData, setDailyWeatherData] = React.useState<weatherInterface>()
	const [pastWeatherData, setPastWeatherData] = React.useState<weatherInterface>()


	const requestToDailyWeather = (requestUrl: string) => {
		return fetch(requestUrl)
		.then(response => {
			if (!response.ok) {        
				setErrorDailyWeather(response.statusText)
			}
			return response.json() 
		})  
		.then(result => { 
			if (result.daily) {
				setErrorDailyWeather('')	
				const shallowCopyResultDaily = [...result.daily]
				const getValueFromParams = Object.values(shallowCopyResultDaily)

				const getDateFromValues:number[] = getValueFromParams.map(item => item.dt);
				const getTempFromValues:number[] = getValueFromParams.map(item => Math.round(item.temp.day));
				const getIconFromValues:string[] = getValueFromParams.map(item => item.weather[0].icon);

				setDailyWeatherData({date: getDateFromValues, temp: getTempFromValues, icon: getIconFromValues})
		
			}
		})
	}

	const requestToPastWeather = (requestUrl: string) => {
		return fetch(requestUrl)
		.then(response => {
			if (!response.ok) { 
				setErrorPastWeather(response.statusText)
			}
			return response.json() 
		})  
		.then(result => { 
			if (result.current) {
				setErrorPastWeather('')
				setPastWeatherData({
					date: result.current.dt, 
					temp: Math.round(result.current.temp), 
					icon: result.current.weather[0].icon
				})
			}
		})
	}


	const showDailyCard = () => {
		if (errorDailyWeather || !dailyWeatherData) {
			return  <Empty/> 
		}
		return  <InnerDaily weatherDataProps={dailyWeatherData} />
	}

	const showPastCard = () => {
		if (errorPastWeather || !pastWeatherData) {
			return  <Empty/> 
		}    
		return  <InnerPast weatherDataProps={pastWeatherData} />
	}


	const imageLeft = { uri: "https://1611lush.github.io/testRepo/static/media/forecast-Bg-buttom.071ea89a.png" };
	const imageRight = { uri: "https://1611lush.github.io/testRepo/static/media/forecast-Bg-up-1.e73b4841.png" };

    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 992px)' })
    const windowWidth = useWindowDimensions().width;


  return ( 

	<ScrollView style={styles.page}>    
		<ImageBackground source={imageLeft} style={styles.image} resizeMode='cover'>
		<ImageBackground source={imageRight} style={styles.image} resizeMode='cover'>
			<Header />
			 		<View style={!isTabletOrMobile ? styles.content : styles.content_mobile}>  
			  			<View style={!isTabletOrMobile ? styles.forecast : styles.forecast_mobile}> 				
					
							<CardDaily 
								title="7 Days Forecast"
								cityArray={cities} 
								apiRequest={requestToDailyWeather} 
								></CardDaily>

							{showDailyCard()}


						</View>

						<View style={!isTabletOrMobile ? styles.forecast : styles.forecast_mobile}> 
						
							<CardPast 
								title="Forecast for a Date in the Past" 
								cityArray={cities}  
								apiRequest={requestToPastWeather}
								></CardPast>

							{showPastCard()}

						</View>

					</View>
	  
	  		<Footer />
	  	</ImageBackground>
	  	</ImageBackground>
	</ScrollView>
  );
}



const styles = StyleSheet.create({
	page: {
		backgroundColor: Variables.accentColor,
	  },
	content: {
	    flexDirection: 'row',
	    flexWrap: 'wrap',
	    justifyContent: 'space-around',
	}, 
	content_mobile: {
	    flexDirection: 'column',	
	    marginLeft: 20,	    
	    marginRight: 20,
	},  
	forecast: {
		backgroundColor: Variables.baseweakColor,
		shadowColor: "#040549",
		shadowOffset: {
			width: 0,
			height: 4,
		},
		shadowOpacity: 0.25,
		shadowRadius: 9.11,
		borderRadius: 8,
		paddingVertical: 50,
		paddingHorizontal: 58,
		maxWidth: 660,
		width: '100%',
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 34,
	},
	forecast_mobile: {
		backgroundColor: Variables.baseweakColor,
		shadowColor: "#040549",
		shadowOffset: {
			width: 0,
			height: 4,
		},
		shadowOpacity: 0.25,
		shadowRadius: 9.11,
		borderRadius: 8,
		paddingVertical: 32,
		paddingHorizontal: 24,
		width: '100%',
		marginBottom: 10,
	},
	image: {
		width: '100%',
		height: 'auto',	
	}
})