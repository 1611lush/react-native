export interface City {
	name: string,
	lat: number,
	lon: number,
}

export const cities: City[] = [
	{
		name: 'Samara',
		lat: 53.195873,
		lon: 50.100193
	},
	{
		name: 'Tolyatti',
		lat: 53.507836,
		lon: 49.420393
	},
	{
		name: 'Saratov',
		lat: 51.533557,
		lon: 46.034257
	},
	{   
		name: 'Kazan',
		lat: 55.796127,
		lon: 49.106405
	},
	{
		name: 'Krasnodar',
		lat: 45.035470,
		lon: 38.975313
	}
]

export interface weatherInterface {
	date: number[] | number,
	temp: number[] | number,
	icon: string[] | string
}


